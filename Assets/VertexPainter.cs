﻿using UnityEngine;
using System.Collections;

public class VertexPainter : MonoBehaviour {
	public Color color = Color.white;

	// Use this for initialization
	void Awake() {
		MeshFilter meshFiler = GetComponent<MeshFilter>();

		if (meshFiler) {
			setColor(meshFiler.mesh); 	
		}
	}

	private void setColor(Mesh mesh) {
		Color[] colors = new Color[mesh.vertices.Length];

		for (int n = 0; n < colors.Length; n++) {
			colors[n] = color;
		}

		mesh.colors = colors;
	}
}
