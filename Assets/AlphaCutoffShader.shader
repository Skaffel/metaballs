﻿Shader "Custom/AlphaCutoffShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_AlphaModifier("Alpha Modifier", Range(0, 1)) = 0.5
		_AlphaCutoff("Alpha Cutoff", Range(0, 1)) = 0
		_AlphaMod("Alpha mod", Range(0, 1)) = 1
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
		}

		Colormask ARGB
		Cull Off
		Lighting Off
		ZWrite Off
		Blend Off

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			sampler2D _MainTex;

			float _AlphaModifier;
			float _AlphaCutoff;
			float _AlphaMod;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;

				return OUT;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord);
				
				c.a = (c.a - _AlphaModifier) / (_AlphaMod - _AlphaModifier);

				return c;
			}
		ENDCG
		}
	}
}
