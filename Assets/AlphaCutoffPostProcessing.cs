﻿using UnityEngine;
using System.Collections;

public class AlphaCutoffPostProcessing : MonoBehaviour {
	public Material material;

	public Material normalMapMaterial;
	public RenderTexture normalMap;

	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		Graphics.SetRenderTarget(destination);
		GL.Clear(true, true, new Color(0, 0, 0, 0));
		Graphics.SetRenderTarget(null);

 		Graphics.Blit(source, destination, material);

 		Graphics.SetRenderTarget(normalMap);
		GL.Clear(true, true, new Color(0, 0, 0, 0));
		Graphics.SetRenderTarget(null);

		Graphics.Blit(destination, normalMap, normalMapMaterial);
	}
}
